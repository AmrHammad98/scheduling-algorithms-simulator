package Final;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class Simulator_Form extends JFrame
{
    //fields
    private JPanel mainPanel;
    private JPanel btnPnl;
    private JPanel ganttPnl;
    private JButton FCFSButton;
    private JButton SRTFButton;
    private JButton LOTTERYButton;
    private JButton BROWSEButton;
    private JPanel tblPnl;
    private JPanel outPnl;
    private JTable table1;
    private JTextField ttFld;
    private JTextField wtFld;
    private JTextField tickFld;
    private JTextField quanFld;
    private JScrollPane scrollPane;


    DefaultTableModel model = new DefaultTableModel();

    private JFileChooser fc = new JFileChooser();

    private String csvFile ;

    private Gantt_Panel gantt= new Gantt_Panel();

    private void drawFCFSgantt()
    {
        int xi = 10 , yj = 15;
        for(Process p : Simulator.FCFS_ready)
        {

            for(int j = p.getStart() ; j < p.getEnd() ;j++ )
            {
                String px = "" + p.getPID();
                gantt.addRect(xi,yj , 10, 30 , px ,p.getColor() );
                xi +=11;
            }
        }
    }

    private void drawSRTgantt()
    {
        int xi = 10 , yj = 15;
        for(Process p : Simulator.Draw)
        {
            String px = "" + p.getPID();
            gantt.addRect(xi,yj , 10, 30 , px ,p.getColor() );
            xi +=11;
        }
    }

    private void drawLotterygantt()
    {
        int xi = 10 , yj = 15;
        for(Process p : Simulator.Draw)
        {
            String px = "" + p.getPID();
            gantt.addRect(xi,yj , 10, 30 , px ,p.getColor() );
            xi +=11;
        }
    }

    private void addRows ()
    {
        Object row[] = new Object[6];
        for(int i = 0 ;i<50 ;i++)
        {
            model.addRow(row);
        }
    }

    private void updateRows(List<Process> list)
    {
        int i=0 , j =0;
        for (Process P:list)
        {
            model.setValueAt(P.getPID() ,i,j);
            j++;
            model.setValueAt(P.getArrival() ,i,j);
            j++;
            if(P.getBurst() == 0)
            {
                model.setValueAt(P.getBurst_temp() , i , j);
            }
            else
            {
                model.setValueAt(P.getBurst() ,i,j);
            }
            j++;
            model.setValueAt(P.getTickets() ,i,j);
            j++;
            model.setValueAt(P.getWaiting() ,i,j);
            j++;
            model.setValueAt(P.getTurnaround() ,i,j);
            i++;
            j=0;
        }
    }

    private void initTable()
    {

        Object [] columns = {"PID" , "Arrival" , "Burst" , "Tickets" , "Waiting" , "Turnaround"};
        model.setColumnIdentifiers(columns);
        table1.setModel(model);
        addRows();
    }

    private void Initialize()
    {
        this.setTitle("Scheduling Simulator");
        this.setSize(1000,800);
        this.setResizable(false);
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);


        this.ganttPnl.setPreferredSize(new Dimension(1000,70));
        this.ganttPnl.add(gantt);


        this.tblPnl.setPreferredSize(new Dimension(500,730));

        this.add(mainPanel);
        this.pack();
    }

    private void chooseFile()
    {
        this.fc.setDialogTitle("Choose CSV File");

        //to choose csv files only
        FileNameExtensionFilter filter = new FileNameExtensionFilter("csv file","csv");
        this.fc.setFileFilter(filter);


        this.fc.setFileSelectionMode(JFileChooser.FILES_ONLY);

        if(fc.showOpenDialog(BROWSEButton) == JFileChooser.APPROVE_OPTION)
        {
            csvFile = fc.getSelectedFile().getAbsolutePath();
            System.out.println(csvFile);
        }
    }

    //constructor
    public Simulator_Form()
    {
        Initialize();
        initTable();
        BROWSEButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                chooseFile();
            }
        });

        LOTTERYButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                if(csvFile == null)
                {
                    JDialog dialog = new JDialog();
                    dialog.setTitle("No File Chosen");
                    dialog.add(new JLabel("Press BROWSE to choose file"));
                    dialog.setSize(200,100);
                    dialog.setResizable(false);
                    dialog.setVisible(true);
                }
                else
                {

                    Simulator.p_in.clear();
                    Simulator.input.clear();
                    gantt.Rectangles.clear();
                    Simulator.copy.clear();
                    Simulator.Draw.clear();

                    Simulator.turn.clear();
                    Simulator.wait.clear();

                    Simulator.readFile(csvFile);
                    Simulator.createProcess();

                    quanFld.setText("" + Simulator.quantum);
                    tickFld.setText("" + Simulator.noTickets );

                    Simulator.Lottery();

                    updateRows(Simulator.copy);

                    wtFld.setText("" + Simulator.calculateWTavg(Simulator.wait));
                    ttFld.setText("" + Simulator.calculateTTavg(Simulator.turn));

                    drawLotterygantt();

                    //Simulator.Lottery();
                    csvFile = null;
                }

            }
        });

        FCFSButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                if(csvFile == null)
                {
                    JDialog dialog = new JDialog();
                    dialog.setTitle("No File Chosen");
                    dialog.add(new JLabel("Press BROWSE to choose file"));
                    dialog.setSize(200,100);
                    dialog.setResizable(false);
                    dialog.setVisible(true);
                }
                else
                {
                    //clear resources previously used
                    Simulator.p_in.clear();
                    Simulator.input.clear();
                    gantt.Rectangles.clear();

                    //read files
                    Simulator.readFile(csvFile);
                    Simulator.createProcess();

                    quanFld.setText("NONE");
                    tickFld.setText("NONE");

                    Simulator.FCFS();
                    drawFCFSgantt();

                    updateRows(Simulator.p_in);

                    wtFld.setText("" + Simulator.calculateWTavg());
                    ttFld.setText("" + Simulator.calculateTTavg());

                    csvFile = null;
                }

            }
        });
        SRTFButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                if(csvFile == null)
                {
                    JDialog dialog = new JDialog();
                    dialog.setTitle("No File Chosen");
                    dialog.add(new JLabel("Press BROWSE to choose file"));
                    dialog.setSize(200,100);
                    dialog.setResizable(false);
                    dialog.setVisible(true);
                }
                else
                {
                    Simulator.p_in.clear();
                    Simulator.input.clear();
                    gantt.Rectangles.clear();
                    Simulator.Draw.clear();
                    Simulator.copy.clear();

                    Simulator.turn.clear();
                    Simulator.wait.clear();
                    //read files
                    Simulator.readFile(csvFile);
                    Simulator.createProcess();

                    quanFld.setText("NONE");
                    tickFld.setText("NONE");

                    Simulator.SRT();

                    updateRows(Simulator.copy);

                    wtFld.setText("" + Simulator.calculateWTavg(Simulator.wait));
                    ttFld.setText("" + Simulator.calculateTTavg(Simulator.turn));

                    drawSRTgantt();


                    csvFile = null;
                }

            }
        });
    }


}
