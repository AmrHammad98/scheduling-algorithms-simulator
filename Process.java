package Final;

//a class that describes a process in an os

import java.awt.*;
import java.util.Random;

public class Process implements Comparable<Process>
{
    //fields
    private int PID;                             // process id
    private int arrival;                        // arrival time
    private int burst;                         // cpu burst
    private int start;                        // start time
    private int end;                         // end time
    private int tickets;                    // number of tickets for lottery
    private int waiting;                    // waiting time of process
    private int turnaround;                // turnaround time of process
    private int burst_temp;               // temporary int that stores the burst used in srt
    private int lowerBound;
    private int upperBound;
    private Color color;

    Random random = new Random();

    //constructor

    public Process(int PID ,int arrival, int burst, int tickets)
    {
        this.PID =   PID;
        this.arrival = arrival;
        this.burst = burst;
        this.tickets = tickets;
        this.burst_temp = this.burst;
        //generate a random color to represent the process
        this.color = new Color(random.nextInt(255),random.nextInt(255),random.nextInt(255));
    }

    //setters and getters

    public void setEnd(int end)
    {
        this.end = end;
    }

    public void setWaiting(int waiting)
    {
        this.waiting = waiting;
    }

    public void setTurnaround(int turnaround)
    {
        this.turnaround = turnaround;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public int getPID()
    {
        return PID;
    }

    public int getArrival()
    {
        return arrival;
    }

    public int getBurst()
    {
        return burst;
    }

    public void setBurst(int burst)
    {
        this.burst = burst;
    }

    public int getStart()
    {
        return start;
    }

    public int getEnd()
    {
        return end;
    }

    public int getTickets()
    {
        return tickets;
    }

    public int getWaiting()
    {
        return waiting;
    }

    public int getTurnaround()
    {
        return turnaround;
    }

    public int getBurst_temp()
    {
        return burst_temp;
    }

    public int getLowerBound() {
        return lowerBound;
    }

    public int getUpperBound() {
        return upperBound;
    }

    public void setLowerBound(int lowerBound) {
        this.lowerBound = lowerBound;
    }

    public void setUpperBound(int upperBound) {
        this.upperBound = upperBound;
    }

    public Color getColor() {
        return color;
    }

    //method


    // a method to compare arrival times of processes will be used for sorting
    @Override
    public int compareTo(Process p )
    {
        if (this.getArrival() == p.getArrival())
        {
            return 0;
        }
        if (this.getArrival() >  p.getArrival())
        {
            return 1;
        }
        return -1;
    }
}
