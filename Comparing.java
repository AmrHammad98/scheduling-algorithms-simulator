package Final;
import java.util.Comparator;


public class Comparing  implements Comparator<Process>
{
    @Override
    public int compare(Process o1, Process o2)
    {
      return o1.getBurst()-o2.getBurst();
    }
}
