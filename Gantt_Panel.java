package Final;

import javafx.scene.shape.Circle;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class Gantt_Panel extends JPanel
{
    List<Rectangle> Rectangles = new ArrayList<>();

    public Gantt_Panel()
    {
        this.setPreferredSize(new Dimension(1000,70));
        this.setBackground(Color.WHITE);
    }

    @Override
    protected void paintComponent(Graphics g)
    {
        super.paintComponent(g);
        for (Rectangle r : Rectangles)
        {
            r.draw(g);
        }
    }

    public void addRect(int x, int y, int width, int height , String desc , Color col) {
        Rectangles.add(new Rectangle( x, y,  width,  height ,  desc ,  col ));
        repaint();
    }
}
