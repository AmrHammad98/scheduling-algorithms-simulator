package Final;

import java.awt.*;

public class Rectangle
{
    //fields
    private int x;
    private int y;
    private int width;
    private int height;
    private String desc;
    private Color col;

    //constructor
    public Rectangle(int x, int y, int width, int height , String desc , Color col)
    {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.desc = desc;
        this.col = col;
    }

    public void draw(Graphics g)
    {

        g.setColor(Color.black);
        g.drawString(this.desc,this.x,this.y- (this.y /2) +2);
        g.setColor(this.col);
        g.fillRect(this.x,this.y,this.width,this.height);


    }
}
