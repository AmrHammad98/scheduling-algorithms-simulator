package Final;

import java.io.*;
import java.util.*;

import static java.lang.Integer.*;

public class Simulator
{
    //fields
    public static List<String> input;                   // input from csv file as string
    public static List<Process> p_in;                   // array list that contains the processes generated from input file

    public static int quantum;                          // quantum beginning of the csv file
    public static int noTickets;                        // total number of tickets


    public static PriorityQueue<Process> FCFS_ready;    // a priority queue that sets priority according to arrival time (dequeue items according to priority)

    public static ArrayList<Integer> wait = new ArrayList<>();
    public static ArrayList<Integer> turn = new ArrayList<>();

    private static ArrayList<Process> finished = new ArrayList<>();
    public static ArrayList<Process>  Draw = new ArrayList<>();
    public static ArrayList<Process>  copy = new ArrayList<>();
    private static Process chosen;


    //constructor
    public Simulator()
    {
        this.input = new ArrayList<String>();
        p_in = new ArrayList<Process>();
    }

    //methods

    //a method that reads from the csv file (to be invoked when read btn in gui is pressed)
    // a file chooser will be used to get the path of the file needed
    public static void readFile(String File)
    {
        try
        {
            Scanner scanner = new Scanner(new File(File));

            while (scanner.hasNext())
            {
                input.add(scanner.nextLine());
            }

            scanner.close();

        }
        catch(Exception e )
        {

        }
    }


    //a method that creates processes from the input from input file
    public static void createProcess ()
    {
        Process P ;
        quantum = Integer.parseInt(input.get(0));
        noTickets = Integer.parseInt(input.get(1));

        for (int i = 2 ; i <= input.size() -1  ;i++)
        {
            String x = input.get(i);
            String[] parts = x.trim().split(",");
            try
            {
                P = new Process(parseInt(parts[0]), parseInt(parts[1]), parseInt(parts[2]), parseInt(parts[3]));
                p_in.add(P);
                copy.add(P);
            }
            catch (NumberFormatException e)
            {
                System.out.println(e.getMessage());
            }
        }

    }

    //function to calculate average waiting time for a scheduling algorithm
    public static float calculateWTavg()
    {
        float avgWT = 0;
        float sum = 0 ;
        for (Process p : p_in)
        {
            sum += p.getWaiting();
        }
        avgWT = sum / p_in.size();
        return avgWT;
    }

    //overloaded function for srt
    public static float calculateWTavg(ArrayList<Integer> arr)
    {
        float avgWT = 0;
        float sum = 0 ;
        for (Integer i : arr)
        {
            sum += i;
        }
        avgWT = sum / arr.size();
        return avgWT;
    }

    //function to calculate average turnaround time for a scheduling algorithm
    public static float calculateTTavg()
    {
        float avgTT = 0;
        float sum = 0 ;
        for (Process p : p_in)
        {
            sum += p.getTurnaround();
        }
        avgTT = sum / p_in.size();
        return avgTT;
    }

    //overloaded function for srt
    public static float calculateTTavg(ArrayList<Integer> arr)
    {
        float avgTT = 0;
        float sum = 0 ;
        for (Integer i : arr)
        {
            sum += i;
        }
        avgTT = sum / arr.size();
        return avgTT;
    }


    // a method that simulates first come first served
    public static void FCFS()
    {
        //add items in a priority queue according to arrival time
        FCFS_ready = new PriorityQueue<Process>();

        for (Process p: p_in)
        {
            FCFS_ready.add(p);
        }

        int i = 0 ;
        int temp_end =0;

        for (Process p : FCFS_ready)
        {
            if(i == 0)
            {
                p.setStart(p.getArrival());
                i =1;
            }
            else
            {
                p.setStart(temp_end);
            }

            p.setEnd(p.getStart() + p.getBurst());
            p.setTurnaround(p.getEnd() - p.getArrival());
            p.setWaiting(p.getStart() - p.getArrival());

            temp_end = p.getEnd();
        }
    }


    //methods related to srt

    //method that finds the shortest that arrived
    private static Process getShortest(List<Process> ap , int timer )
    {
        for(Process p : ap )
        {
            if(p.getArrival() <= timer)
            {
                return p;
            }
        }
        return null;
    }

    private static int isVisited(Process x , ArrayList<Process> ap)
    {
        for (Process P: ap)
        {
            if(x.getPID() == P.getPID())
            {
                return 1;
            }
        }
        return 0;
    }

    //function to calculate waiting time for srt
    private static int calculateWaiting(int fin , int b ,int arr )
    {
        int wait =0;
        wait = fin - b -arr;

        if (wait <0 )
        {
            return 0 ;
        }
        else
        {
            return wait;
        }
    }

    public static void SRT()
    {
        int timer = 0;
        int complete = 0;
        Process temp = null;
        ArrayList<Process> visited = new ArrayList<>();


        complete = p_in.size();

        while(complete > 0 )                                                  // while all processes has not finished
        {
            p_in.sort(new Comparing());


            temp = p_in.get(0);
            if (temp.getArrival() > timer )                                   // temp is the shortest but not yet arrived
            {
               temp = getShortest(p_in, timer) ;
            }


            //set start time
            if(isVisited( temp , visited ) == 0 )
            {
                temp.setStart(timer);
                visited.add(temp);
            }

            if(temp.getArrival() <= timer)
            {
                //if the process has finished
                if(temp.getBurst() == 0)
                {
                    //decrement number of processes
                    complete--;

                    //calculate finish time , turnaround time , waiting time of process
                    temp.setEnd(timer);
                    temp.setTurnaround(temp.getEnd() - temp.getArrival());
                    temp.setWaiting(calculateWaiting(temp.getEnd(),temp.getBurst_temp(),temp.getArrival()));

                    wait.add(temp.getWaiting());
                    turn.add(temp.getTurnaround());

                    p_in.remove(temp);
                }
                else
                {
                    temp.setBurst(temp.getBurst()-1);
                    Draw.add(temp);
                    timer++;
                }
            }
        }

    }

    //methods related to lottery

    //method to generate a random number
    private static int generateRandom(int ticks)
    {
        Random random = new Random();
        return random.nextInt(ticks);
    }

    //method to calculate bounds for each process (range of tickets)
    private static void calculateRange(List <Process> l)
    {
        int temp = 0;
        int i = 0;
        while (temp != noTickets && i < l.size() )
        {
            l.get(i).setLowerBound(temp);
            l.get(i).setUpperBound( l.get(i).getLowerBound() + ( l.get(i).getTickets()+ -1 ) );
            temp = l.get(i).getUpperBound() + 1 ;

            //just for testing
            //System.out.println(l.get(i).getLowerBound());
            //System.out.println(l.get(i).getUpperBound());

            i++;
        }
    }


    private static int get_totalBurst (List <Process> l)
    {
        int total = 0;
        for (Process p : l)
        {
            total += p.getBurst();
        }
        return total;
    }

    //method to check if the drawn number is in the range of tickets owned by the process
    private static boolean isinRange(int lb , int ub , int no)
    {
        if(no >= lb && no <= ub)
        {
            return  true;
        }
        return false;
    }

    //method that returns the process with drawn ticket in its range
    private static Process drawProcess(List<Process> l , int rand)
    {
        for (Process p: l)
        {
            if(p.getBurst() != 0 )
            {
                if(isinRange(p.getLowerBound() , p.getUpperBound() , rand)  && isVisited(p,finished) ==0 )
                {
                  return p;
                }
            }
        }
        return null;
    }

    //lottery scheduling algorithm
    public static void Lottery( )
    {
        int totalBurst = get_totalBurst(p_in);
        int random = 0;
        int timer = 0;

        boolean flag  = false;

        chosen = null;

        //set lower bound and upper bound of each process of tickets
        calculateRange(p_in);

        while (  totalBurst > 0 )
        {
            //choose randomly a number of ticket
            random = generateRandom(noTickets);

            chosen = drawProcess(p_in , random);

            if(chosen == null)
            {
                continue;
            }

            int remaining = 0;
            //if remaining burst < quantum (process will obviously finish)
            if((chosen.getBurst() - quantum) < 0 )
            {
                //finish process
                remaining = chosen.getBurst();

                chosen.setEnd(timer + remaining);
                chosen.setTurnaround(chosen.getEnd() -chosen.getArrival());
                chosen.setWaiting(calculateWaiting(chosen.getEnd(),chosen.getBurst_temp(),chosen.getArrival()));

                //for calculating average
                turn.add(chosen.getTurnaround());
                wait.add(chosen.getWaiting());


                finished.add(chosen);
                p_in.remove(chosen);


                chosen.setBurst(0);
                flag = true ;
            }
            else
            {
                //process not yet finished
                chosen.setBurst(chosen.getBurst() - quantum);
            }
            if(flag == true)
            {
                for (int i = 0 ;i< remaining  ;i++)
                {
                    //System.out.print(chosen.getPID() + "-");
                    Draw.add(chosen);
                }

                timer += remaining;
                totalBurst -= remaining;
                flag = false;
            }
            else
                {
                    for (int i = 0 ;i< quantum  ;i++)
                    {
                        //System.out.print(chosen.getPID() + "-");
                        Draw.add(chosen);
                    }

                    timer += quantum;
                    totalBurst -= quantum;
                }

        }
    }
}



